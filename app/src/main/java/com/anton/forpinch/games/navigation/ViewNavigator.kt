package com.anton.forpinch.games.navigation

import androidx.navigation.Navigation
import com.anton.forpinch.games.AppActivity

class ViewNavigator(private val activity: AppActivity) {

    fun navigate(action: NavigatorAction) {
        when (action) {
            is NavigatorAction.Forward -> {
                Navigation.findNavController(activity, action.frame)
                        .navigate(action.action, action.bundle, action.navOptions)
            }
            is NavigatorAction.Back -> {
                Navigation.findNavController(activity, action.frame)
                        .popBackStack()
            }
            is NavigatorAction.BackTo -> {
                Navigation.findNavController(activity, action.frame)
                        .popBackStack(action.destination, action.inclusive)
            }
        }
    }

}