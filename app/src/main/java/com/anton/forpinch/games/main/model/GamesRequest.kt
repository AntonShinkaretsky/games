package com.anton.forpinch.games.main.model

import com.google.gson.annotations.SerializedName

data class GamesRequest(
        @SerializedName("offset") var offset: Int,
        @SerializedName("limit") var limit: Int,
        @SerializedName("fields") var fields: String? = "*, cover.*"
) {

    fun createRequestBody(): Map<String, String> {

        val map = HashMap<String, String>()

        val offset = offset
        val limit = limit
        val fields = fields

        map["offset"] = offset.toString()
        map["limit"] = limit.toString()
        if (fields != null) map["fields"] = fields.toString()

        return map
    }

}