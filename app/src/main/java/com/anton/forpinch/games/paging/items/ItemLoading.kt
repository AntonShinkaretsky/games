package com.anton.forpinch.games.paging.items

import com.anton.forpinch.games.main.list.adapter.GamesItem
import com.anton.forpinch.games.paging.Paging.ID_LOADING

class ItemLoading : GamesItem {

    override val id: Int
        get() = ID_LOADING

    override fun equals(other: Any?): Boolean {
        return other is ItemLoading
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}