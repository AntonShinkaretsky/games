package com.anton.forpinch.games.main.list.repo

import androidx.paging.PagedList
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.anton.forpinch.games.main.GamesService
import com.anton.forpinch.games.main.model.Game
import com.anton.forpinch.games.main.model.GamesRequest
import com.anton.forpinch.games.paging.Paging.INITIAL_OFFSET
import com.anton.forpinch.games.paging.Paging.LOAD_SIZE
import com.anton.forpinch.games.paging.PagingRequestHelper
import com.anton.forpinch.games.utils.DataState
import com.anton.forpinch.games.utils.DataState.Companion.STATE_OFFSET_LIMIT
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.util.concurrent.Executor

class GamesBoundaryCallback(
        private val service: GamesService,
        private val ioExecutor: Executor,
        private val handleResponse: (List<Game>) -> Unit
) : PagedList.BoundaryCallback<Game>() {

    // The boundary callback might be called multiple times for the same direction so it does its own
    // rate limiting using the PagingRequestHelper class.
    val helper = PagingRequestHelper(ioExecutor)
    val networkState = helper.createLiveState()

    private var getGamesDisposable: Disposable? = null

    @MainThread
    override fun onZeroItemsLoaded() {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL) { helper ->
            val request = GamesRequest(INITIAL_OFFSET, LOAD_SIZE).createRequestBody()
            helper.createRequest(request)
        }
    }

    @MainThread
    override fun onItemAtEndLoaded(itemAtEnd: Game) {
        helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER) { helper ->
            val request = GamesRequest(itemAtEnd.indexInResponse, LOAD_SIZE).createRequestBody()
            helper.createRequest(request)
        }
    }

    private fun PagingRequestHelper.Request.Callback.createRequest(request: Map<String, String>) {
        getGamesDisposable = service.getGames(request)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { response ->
                            getGamesDisposable?.dispose()
                            handleResponse(this, response)
                        },
                        { t ->
                            getGamesDisposable?.dispose()
                            handleResponseError(this, t)
                        }
                )
    }

    override fun onItemAtFrontLoaded(itemAtFront: Game) {
        // Ignored, since we only ever append to what's in the DB
    }

    // Every time we gets new items, boundary callback simply inserts them into the database
    // Paging library takes care of refreshing the list if necessary.
    private fun handleResponse(it: PagingRequestHelper.Request.Callback, response: List<Game>) {
        ioExecutor.execute {
            handleResponse(response)
            it.recordSuccess()
        }
    }

    private fun handleResponseError(it: PagingRequestHelper.Request.Callback, throwable: Throwable) {
        it.recordFailure(throwable)
    }

    private fun PagingRequestHelper.createLiveState(): LiveData<DataState> {
        val liveData = MutableLiveData<DataState>()
        addListener { report ->
            when {
                report.hasRunning() -> liveData.postValue(DataState.Loading())
                report.hasError() -> liveData.postError(report.getThrowable())
                else -> liveData.postValue(DataState.Data(null)) // Because we don't pass data through state here
            }
        }
        return liveData
    }

    private fun MutableLiveData<DataState>.postError(throwable: Throwable) {
        val state = when {
            throwable.isOffsetLimitation() -> DataState.Error(throwable, STATE_OFFSET_LIMIT)
            else -> DataState.Error(throwable)
        }
        postValue(state)
    }

    private fun Throwable.isOffsetLimitation(): Boolean {
        return this is HttpException &&
                this.code() == 403 &&
                this.response().errorBody()?.string()?.contains("Request offset to high") ?: false
    }

    private fun PagingRequestHelper.StatusReport.getThrowable(): Throwable {
        return PagingRequestHelper.RequestType.values().mapNotNull {
            getErrorFor(it)
        }.first()
    }

}