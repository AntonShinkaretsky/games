package com.anton.forpinch.games.main.details

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.anton.forpinch.games.main.model.Game
import com.anton.forpinch.games.main.navigation.GamesNavigator
import com.anton.forpinch.games.main.details.repo.GameDetailsRepository

class GameDetailsViewModel(
        private val navigator: GamesNavigator,
        private val repo: GameDetailsRepository
) : ViewModel() {

    val gameLive = MediatorLiveData<Game>()

    fun initialize(gameId: Int) {
        gameLive.addSource(repo.getGame(gameId)) { result: Game? ->
            result?.let { gameLive.value = result }
        }
    }

    fun moveGameDetailsBackToList() {
        navigator.moveGameDetailsBackToList()
    }

}