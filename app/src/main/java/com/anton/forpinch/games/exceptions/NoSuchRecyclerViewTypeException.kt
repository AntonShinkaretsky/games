package com.anton.forpinch.games.exceptions

import java.lang.RuntimeException

class NoSuchRecyclerViewTypeException : RuntimeException()