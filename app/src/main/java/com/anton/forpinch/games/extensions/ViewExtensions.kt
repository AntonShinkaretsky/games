package com.anton.forpinch.games.extensions

import android.view.View
import android.util.TypedValue

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}