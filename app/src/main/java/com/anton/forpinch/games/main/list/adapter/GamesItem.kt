package com.anton.forpinch.games.main.list.adapter

interface GamesItem {
     val id: Int
     override fun equals(other: Any?): Boolean
}