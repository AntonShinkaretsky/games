package com.anton.forpinch.games.utils

import android.content.Context
import android.content.Intent
import android.content.Intent.*
import android.net.Uri
import android.content.pm.PackageManager
import android.util.Log
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import com.anton.forpinch.games.R

const val TAG = "IntentUtils"

fun Context.openLinkWithBrowser(url: String) {
    val intent = Intent(ACTION_VIEW)
    intent.data = Uri.parse(url)

    if (intent.isAvailable(this)) startActivity(intent)
    else showIntentErrorToast(getString(R.string.toast_no_browser))
}

private fun Intent.isAvailable(context: Context): Boolean {
    val mgr = context.packageManager
    val list = mgr.queryIntentActivities(this, PackageManager.MATCH_DEFAULT_ONLY)
    return list.size > 0
}

private fun Context.showIntentErrorToast(error: String) {
    Log.e(TAG, error)
    Toast.makeText(this, error, LENGTH_SHORT).show()
}