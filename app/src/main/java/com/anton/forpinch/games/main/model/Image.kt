package com.anton.forpinch.games.main.model

import com.google.gson.annotations.SerializedName

data class Image(
        @SerializedName("id") val elementId: Int?, // 22963
        @SerializedName("image_id") val imageHash: String?, // "a3u3ch20yoxshrwwykz4"
        @SerializedName("url") val imageUrl: String? // "//images.igdb.com/igdb/image/upload/t_thumb/a3u3ch20yoxshrwwykz4.jpg"
)