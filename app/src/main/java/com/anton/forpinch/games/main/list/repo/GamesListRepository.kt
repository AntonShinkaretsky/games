package com.anton.forpinch.games.main.list.repo

import com.anton.forpinch.games.main.list.adapter.GamesItem
import com.anton.forpinch.games.paging.Listing

interface GamesListRepository {

    fun getGames(): Listing<GamesItem>

}
