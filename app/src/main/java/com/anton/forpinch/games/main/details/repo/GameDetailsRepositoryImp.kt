package com.anton.forpinch.games.main.details.repo

import androidx.lifecycle.LiveData
import com.anton.forpinch.games.main.model.Game
import com.anton.forpinch.games.room.AppDatabase

class GameDetailsRepositoryImp(private val db: AppDatabase) : GameDetailsRepository {

    override fun getGame(gameId: Int): LiveData<Game> {
        return db.gamesDao().get(gameId)
    }

}