package com.anton.forpinch.games.networking

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object Retrofit {

    val instance: Retrofit by lazy { initRetrofit() }

    const val API_KEY_LABEL = "user-key"
    const val API_KEY = "bdaad8002d05210d49196af359e2725e"

    private const val BASE_URL = "https://api-v3.igdb.com/"

    private fun initRetrofit(): Retrofit {

        // In real application I would setup I for debug version only. Probably with flavours
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(MainHeadersInterceptor())
                .build()

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build()
    }

}