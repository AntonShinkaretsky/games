package com.anton.forpinch.games

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.anton.forpinch.games.navigation.AppNavigator
import com.anton.forpinch.games.navigation.ViewNavigator
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class AppActivity : AppCompatActivity() {

    private val appNavigator: AppNavigator by inject()
    private val viewNavigator: ViewNavigator by inject { parametersOf(this@AppActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)
    }

    override fun onStart() {
        super.onStart()
        appNavigator.bindViewNavigator(viewNavigator)
    }

    override fun onStop() {
        super.onStop()
        appNavigator.unbindViewNavigator()
    }

}
