package com.anton.forpinch.games.main.list.repo

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.anton.forpinch.games.main.GamesService
import com.anton.forpinch.games.main.list.adapter.GamesItem
import com.anton.forpinch.games.main.model.Game
import com.anton.forpinch.games.main.model.GamesRequest
import com.anton.forpinch.games.paging.Listing
import com.anton.forpinch.games.paging.Paging
import com.anton.forpinch.games.paging.Paging.INITIAL_OFFSET
import com.anton.forpinch.games.paging.Paging.LOAD_SIZE
import com.anton.forpinch.games.room.AppDatabase
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors

class GamesListRepositoryImp(
        private val service: GamesService,
        private val db: AppDatabase
) : GamesListRepository {

    private var getGamesDisposable: Disposable? = null
    private val diskIo = Executors.newSingleThreadExecutor()

    @MainThread
    override fun getGames(): Listing<GamesItem> {

        val boundaryCallback = createBoundaryCallback()

        // Mutable live data to trigger refresh requests which eventually calls refresh method and gets a new live data
        // Each refresh request by the user becomes a newly dispatched data in refreshTrigger
        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            refresh()
        }

        return Listing(
                pagedList = createLivePagedList(boundaryCallback),
                networkState = boundaryCallback.networkState,
                refreshState = refreshState,
                refresh = { refreshTrigger.value = null },
                retry = { boundaryCallback.helper.retryAllFailed() }
        )
    }

    private fun createBoundaryCallback(): GamesBoundaryCallback {
        return GamesBoundaryCallback(
                service = service,
                ioExecutor = diskIo,
                handleResponse = this::insertResultIntoDb
        )
    }

    private fun createLivePagedList(boundaryCallback: GamesBoundaryCallback): LiveData<PagedList<GamesItem>> {
        return db.gamesDao().getAllSourceFactory().toLiveData(
                config = Paging.config,
                boundaryCallback = boundaryCallback
        ) as LiveData<PagedList<GamesItem>> // We know for sure here, that Game implements GamesItem
    }

    @MainThread
    private fun refresh(): LiveData<Boolean> {

        val refreshState = MutableLiveData<Boolean>()
        refreshState.value = true

        val refreshRequest = GamesRequest(INITIAL_OFFSET, LOAD_SIZE).createRequestBody()
        getGamesDisposable = service.getGames(refreshRequest)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            getGamesDisposable?.dispose()

                            // Since the PagedList already uses a database bound data source, it will automatically be
                            // updated after the database transaction is finished.
                            diskIo.execute { clearAllAndInsert(it) }
                            refreshState.postValue(false)
                        },
                        {
                            getGamesDisposable?.dispose()
                            refreshState.postValue(false)
                        }
                )

        return refreshState
    }

    private fun insertResultIntoDb(games: List<Game>) {
        db.runInTransaction {
            val start = db.gamesDao().getNextIndexInGames()
            val items = games.mapIndexed { index, child ->
                child.indexInResponse = start + index
                child
            }
            db.gamesDao().insert(items)
        }
    }

    private fun clearAllAndInsert(games: List<Game>) {
        db.runInTransaction {
            db.gamesDao().deleteAll()
            insertResultIntoDb(games)
        }
    }

}