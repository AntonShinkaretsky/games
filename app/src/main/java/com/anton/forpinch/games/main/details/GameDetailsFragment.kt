package com.anton.forpinch.games.main.details

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.anton.forpinch.games.R
import com.anton.forpinch.games.extensions.createNewStub
import com.anton.forpinch.games.extensions.gone
import com.anton.forpinch.games.extensions.show
import com.anton.forpinch.games.images.loadHeaderImage
import com.anton.forpinch.games.main.list.GamesListFragment.Companion.KEY_GAME_ID
import com.anton.forpinch.games.main.model.Game
import com.anton.forpinch.games.utils.openLinkWithBrowser
import com.anton.forpinch.games.utils.setDebounceOnClickListener
import kotlinx.android.synthetic.main.fragment_game_details.*
import kotlinx.android.synthetic.main.stub_unexpected_error.*
import org.koin.android.viewmodel.ext.android.viewModel

class GameDetailsFragment : Fragment() {

    private val vm by viewModel<GameDetailsViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_game_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initialize()
        observe()
    }

    private fun initialize() {
        val gameId = arguments?.getInt(KEY_GAME_ID)
        if (gameId != null) vm.initialize(gameId)
        else showUnexpectedError()

        toolbar_game_details.setNavigationOnClickListener {
            vm.moveGameDetailsBackToList()
        }
    }

    private fun observe() {
        vm.gameLive.observe(viewLifecycleOwner, Observer { game ->
            game?.let { bind(it) }
        })
    }

    private fun bind(game: Game) {
        val hash = game.cover?.imageHash
        if (hash != null) image_game_details_header.loadHeaderImage(hash)

        text_title.text = game.name

        val summary = game.summary
        if (summary != null) text_summary.text = summary
        else text_summary.gone()

        game.url?.let { url ->
            button_visit.setDebounceOnClickListener { context?.openLinkWithBrowser(url) }
        }
    }

    private fun showUnexpectedError() {
        layout_game_details.createNewStub(R.layout.stub_unexpected_error, R.id.layout_unknown_error)
        button_close_unknown_error.setDebounceOnClickListener {
            vm.moveGameDetailsBackToList()
        }
        layout_unknown_error.show()
        layout_unknown_error.bringToFront()
    }

}