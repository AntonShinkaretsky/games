package com.anton.forpinch.games.main

import com.anton.forpinch.games.main.model.Game
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface GamesService {

    @GET("games")
    fun getGames(@QueryMap query: Map<String, String>): Single<List<Game>>

}