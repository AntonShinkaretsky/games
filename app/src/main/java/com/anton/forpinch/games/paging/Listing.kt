package com.anton.forpinch.games.paging

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.anton.forpinch.games.utils.DataState

data class Listing<T>(
        val pagedList: LiveData<PagedList<T>>,
        val networkState: LiveData<DataState>,
        val refreshState: LiveData<Boolean>,
        val refresh: () -> Unit,
        val retry: () -> Unit
)