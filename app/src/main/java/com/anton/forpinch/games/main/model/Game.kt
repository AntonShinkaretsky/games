package com.anton.forpinch.games.main.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.anton.forpinch.games.main.list.adapter.GamesItem
import com.anton.forpinch.games.utils.AdapterClick
import com.google.gson.annotations.SerializedName

@Entity
data class Game(
        @PrimaryKey
        @SerializedName("id") override val id: Int, // 1234,
        @SerializedName("name") val name: String?, // "Test game",
        @SerializedName("summary") val summary: String?, // "Test game",
        @SerializedName("slug") val slug: String?, // "/games/test-game",
        @SerializedName("url") val url: String?, // "https://www.igdb.com/games/test-game",
        @Embedded
        @SerializedName("cover") val cover: Image? // [...]
) : GamesItem, AdapterClick {

    var indexInResponse: Int = -1

}