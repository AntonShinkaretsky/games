package com.anton.forpinch.games.utils

import android.view.View

fun View.setDebounceOnClickListener(onClick: (View) -> Unit) {
    setOnClickListener(object : View.OnClickListener {

        private val debounce = 1000
        private var lastClick = 0L

        override fun onClick(view: View) {
            val currentClick = System.currentTimeMillis()

            if (currentClick.debounceWaited()) {
                onClick.invoke(view)
                lastClick = currentClick
            }
        }

        private fun Long.debounceWaited(): Boolean {
            return this > lastClick + debounce
        }
    })
}