package com.anton.forpinch.games.main.navigation

import android.os.Bundle

interface GamesNavigator {

    fun moveGamesListToDetails(bundle: Bundle)
    fun moveGameDetailsBackToList()

}