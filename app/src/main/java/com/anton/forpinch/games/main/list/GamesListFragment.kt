package com.anton.forpinch.games.main.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.anton.forpinch.games.R
import com.anton.forpinch.games.main.list.adapter.GamesListAdapter
import com.anton.forpinch.games.main.list.adapter.GamesOffsetDecorator
import com.anton.forpinch.games.main.model.Game
import com.anton.forpinch.games.utils.ActionWithTag
import com.anton.forpinch.games.utils.ActionWithTag.Companion.ACTION_REFRESH
import com.anton.forpinch.games.utils.DataState
import com.anton.forpinch.games.utils.AdapterClick
import com.anton.forpinch.games.utils.AdapterListener
import kotlinx.android.synthetic.main.fragment_games_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class GamesListFragment : Fragment(), AdapterListener {

    private val vm by viewModel<GamesListViewModel>()
    private val listAdapter by lazy { GamesListAdapter(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_games_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initialize()
        observe()
    }

    private fun initialize() {
        initializeRecycler()
        refresh_games.setOnRefreshListener {
            vm.refresh()
        }
    }

    private fun initializeRecycler() {
        recycler_games.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
            addItemDecoration(GamesOffsetDecorator())
        }
    }

    private fun observe() {
        vm.posts.observe(this, Observer {
            listAdapter.submitList(it)
        })

        vm.refreshState.observe(this, Observer { refreshing ->
            refresh_games.isRefreshing = refreshing ?: false
        })

        vm.networkState.observe(this, Observer { state ->
            when (state) {
                is DataState.Data<*> -> handleStateData()
                is DataState.Loading -> handleStateLoading()
                is DataState.Error -> handleStateError(state.type)
            }
        })
    }

    private fun handleStateData() {
        showLoading(false)
        showError(false)
        showOffsetLimitation(false)
    }

    private fun handleStateLoading() {
        showLoading(true)
        showError(false)
        showOffsetLimitation(false)
    }

    private fun handleStateError(type: Int) {
        if (type == DataState.STATE_OFFSET_LIMIT) {
            showLoading(false)
            showError(false)
            showOffsetLimitation(true)
        } else {
            showLoading(false)
            showError(true)
            showOffsetLimitation(false)
        }
    }

    private fun showLoading(visible: Boolean) {
        listAdapter.setLoadingItem(visible)
    }

    private fun showError(visible: Boolean) {
        listAdapter.setErrorItem(visible)
    }

    private fun showOffsetLimitation(visible: Boolean) {
        listAdapter.setOffsetLimitationItem(visible)
    }

    override fun listen(click: AdapterClick?) {
        when (click) {
            is Game -> openGame(click)
            is ActionWithTag -> if (click.tag == ACTION_REFRESH) vm.retry()
        }
    }

    private fun openGame(game: Game) {
        val bundle = Bundle(1)
        bundle.putInt(KEY_GAME_ID, game.id)
        vm.moveGamesListToDetails(bundle)
    }

    companion object {
        const val KEY_GAME_ID = "key_game_id"
    }

}