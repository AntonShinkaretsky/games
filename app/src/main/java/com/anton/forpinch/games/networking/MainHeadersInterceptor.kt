package com.anton.forpinch.games.networking

import com.anton.forpinch.games.networking.Retrofit.API_KEY
import com.anton.forpinch.games.networking.Retrofit.API_KEY_LABEL
import okhttp3.Interceptor
import okhttp3.Response

class MainHeadersInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request()

        val request = original.newBuilder()
                .header(API_KEY_LABEL, API_KEY)
                .header("Accept", "application/json")
                .method(original.method(), original.body())
                .build()

        return chain.proceed(request)
    }

}