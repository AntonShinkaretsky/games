package com.anton.forpinch.games.main.list.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.anton.forpinch.games.utils.convertDpToPixel

class GamesOffsetDecorator : RecyclerView.ItemDecoration() {

    private val top = convertDpToPixel(8)
    private val sides = convertDpToPixel(8)
    private val bottom = convertDpToPixel(8)

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        when (parent.getChildAdapterPosition(view)) {
            0 -> outRect.set(sides, top, sides, bottom)
            else -> outRect.set(sides, 0, sides, bottom)
        }
    }

}