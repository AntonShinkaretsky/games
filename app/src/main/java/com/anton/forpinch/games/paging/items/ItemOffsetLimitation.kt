package com.anton.forpinch.games.paging.items

import com.anton.forpinch.games.main.list.adapter.GamesItem
import com.anton.forpinch.games.paging.Paging

class ItemOffsetLimitation : GamesItem {

    override val id: Int
        get() = Paging.ID_OFFSET_LIMITATION

    override fun equals(other: Any?): Boolean {
        return other is ItemLoading
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}