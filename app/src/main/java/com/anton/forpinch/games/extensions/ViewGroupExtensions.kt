package com.anton.forpinch.games.extensions

import android.view.ViewGroup
import android.view.ViewStub
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes

// Sometimes some phones, for example Meizu M2 note, sets id=0 to root view,
// if you inflate it in a runtime added ViewStub, so you should add this ID manually
// I don't give a shit why
fun ViewGroup.createNewStub(@LayoutRes layout: Int, @IdRes inflatedRoot: Int) {
    val stub = ViewStub(context)
    addView(stub)
    stub.layoutResource = layout
    stub.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
    stub.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
    stub.inflatedId = inflatedRoot
    stub.inflate()
}