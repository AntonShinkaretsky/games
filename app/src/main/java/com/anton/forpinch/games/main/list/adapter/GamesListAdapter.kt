package com.anton.forpinch.games.main.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.anton.forpinch.games.R
import com.anton.forpinch.games.exceptions.NoSuchRecyclerItemTypeException
import com.anton.forpinch.games.exceptions.NoSuchRecyclerViewTypeException
import com.anton.forpinch.games.main.model.Game
import com.anton.forpinch.games.paging.items.*
import com.anton.forpinch.games.utils.ActionWithTag
import com.anton.forpinch.games.utils.ActionWithTag.Companion.ACTION_REFRESH
import com.anton.forpinch.games.utils.AdapterListener
import com.anton.forpinch.games.utils.setDebounceOnClickListener
import kotlinx.android.synthetic.main.item_error.view.*
import kotlinx.android.synthetic.main.item_game.view.*

class GamesListAdapter(private val listener: AdapterListener)
    : PagedListAdapter<GamesItem, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    private var loading = false
    private var error = false
    private var offsetLimitation = false

    private val itemError: GamesItem
        get() = ItemError()

    private val itemLoading: GamesItem
        get() = ItemLoading()

    private val itemOffsetLimitation: GamesItem
        get() = ItemOffsetLimitation()

    fun setLoadingItem(visible: Boolean) {
        loading = visible
        notifyItemChanged(currentList?.size ?: 0)
    }

    fun setErrorItem(visible: Boolean) {
        error = visible
        notifyItemChanged(currentList?.size ?: 0)
    }

    fun setOffsetLimitationItem(visible: Boolean) {
        offsetLimitation = visible
        notifyItemChanged(currentList?.size ?: 0)
    }

    // If footer has to be displayed, add 1 to item count
    override fun getItemCount(): Int {
        return super.getItemCount().incrementIf { loading || error || offsetLimitation }
    }

    private fun Int.incrementIf(predicate: () -> Boolean): Int {
        return if (predicate()) this + 1
        else this
    }

    // If position is more than real count, return item, based on state
    override fun getItem(position: Int): GamesItem? {
        val realCount = super.getItemCount()

        // Position + 1 because starts from 0, but itemCount starts from 1
        return if (position + 1 > realCount) {
            when {
                loading -> itemLoading
                error -> itemError
                offsetLimitation -> itemOffsetLimitation
                else -> itemLoading // Should never happen
            }
        } else {
            super.getItem(position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return CellType.of(item).type()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CellType.of(viewType).holder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        CellType.of(item)
                .bind(holder, item, listener)
    }

    private enum class CellType {
        GAME {
            override fun belongsTo(item: GamesItem?): Boolean = item is Game
            override fun type(): Int = R.layout.item_game
            override fun holder(parent: ViewGroup): RecyclerView.ViewHolder {
                return GameViewHolder(parent.viewOf(type()))
            }

            override fun bind(holder: RecyclerView.ViewHolder, item: Any?, listener: AdapterListener) {
                if (holder is GameViewHolder && item is Game) {
                    holder.bind(item)
                    holder.itemView.layout_game.apply {
                        setDebounceOnClickListener { listener.listen(item) }
                    }
                }
            }
        },
        LOADING {
            override fun belongsTo(item: GamesItem?): Boolean = item is ItemLoading
            override fun type(): Int = R.layout.item_loading
            override fun holder(parent: ViewGroup): RecyclerView.ViewHolder {
                return LoadingViewHolder(parent.viewOf(type()))
            }

            override fun bind(holder: RecyclerView.ViewHolder, item: Any?, listener: AdapterListener) {
                // No info is needed to be bind
            }
        },
        ERROR {
            override fun belongsTo(item: GamesItem?): Boolean = item is ItemError
            override fun type(): Int = R.layout.item_error
            override fun holder(parent: ViewGroup): RecyclerView.ViewHolder {
                return ErrorViewHolder(parent.viewOf(type()))
            }

            override fun bind(holder: RecyclerView.ViewHolder, item: Any?, listener: AdapterListener) {
                if (holder is ErrorViewHolder) {
                    holder.itemView.text_pagination_error.setDebounceOnClickListener {
                        listener.listen(ActionWithTag(ACTION_REFRESH))
                    }
                }
            }
        },
        OFFSET_LIMITATION {
            override fun belongsTo(item: GamesItem?): Boolean = item is ItemOffsetLimitation
            override fun type(): Int = R.layout.item_offset_limitation
            override fun holder(parent: ViewGroup): RecyclerView.ViewHolder {
                return OffsetLimitationViewHolder(parent.viewOf(type()))
            }

            override fun bind(holder: RecyclerView.ViewHolder, item: Any?, listener: AdapterListener) {
                // No info is needed to be bind
            }
        };

        internal abstract fun belongsTo(item: GamesItem?): Boolean
        internal abstract fun type(): Int
        internal abstract fun holder(parent: ViewGroup): RecyclerView.ViewHolder
        internal abstract fun bind(holder: RecyclerView.ViewHolder, item: Any?, listener: AdapterListener)

        protected fun ViewGroup.viewOf(@LayoutRes resource: Int): View {
            return LayoutInflater
                    .from(context)
                    .inflate(resource, this, false)
        }

        companion object {

            fun of(item: GamesItem?): CellType {
                for (cellType in values()) {
                    if (cellType.belongsTo(item)) return cellType
                }
                throw NoSuchRecyclerItemTypeException()
            }

            fun of(viewType: Int): CellType {
                for (cellType in values()) {
                    if (cellType.type() == viewType) return cellType
                }
                throw NoSuchRecyclerViewTypeException()
            }
        }
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<GamesItem> = object : DiffUtil.ItemCallback<GamesItem>() {

            override fun areItemsTheSame(oldItem: GamesItem, newItem: GamesItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: GamesItem, newItem: GamesItem): Boolean {
                return oldItem == newItem
            }
        }
    }
}