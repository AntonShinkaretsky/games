package com.anton.forpinch.games.main.details.repo

import androidx.lifecycle.LiveData
import com.anton.forpinch.games.main.model.Game

interface GameDetailsRepository {

    fun getGame(gameId: Int): LiveData<Game>

}