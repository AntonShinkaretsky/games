package com.anton.forpinch.games.main.navigation

import android.os.Bundle
import com.anton.forpinch.games.R
import com.anton.forpinch.games.navigation.AppNavigator
import com.anton.forpinch.games.navigation.NavigatorAction

class GamesNavigatorImp(private val appNavigator: AppNavigator) : GamesNavigator {

    override fun moveGamesListToDetails(bundle: Bundle) {
        val action = NavigatorAction.Forward(
                R.id.appFragmentContainer,
                R.id.action_gamesListFragment_to_gameDetailsFragment,
                bundle
        )
        appNavigator.navigate(action)
    }

    override fun moveGameDetailsBackToList() {
        val action = NavigatorAction.Back(
                R.id.appFragmentContainer
        )
        appNavigator.navigate(action)
    }

}