package com.anton.forpinch.games.utils

import android.content.res.Resources
import kotlin.math.roundToInt

fun convertPixelsToDp(px: Int): Int {
    val dp = px / (Resources.getSystem().displayMetrics.densityDpi / 160f)
    return dp.roundToInt()
}

fun convertDpToPixel(dp: Int): Int {
    val px = dp * (Resources.getSystem().displayMetrics.densityDpi / 160f)
    return px.roundToInt()
}
