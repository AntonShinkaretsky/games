package com.anton.forpinch.games.utils

sealed class DataState {

    class Initial(val type: Int = STATE_DEFAULT) : DataState()
    class Data<M>(val data: M, val type: Int = STATE_DEFAULT) : DataState()
    class Loading(val type: Int = STATE_DEFAULT) : DataState()
    class Error(val throwable: Throwable, val type: Int = STATE_DEFAULT) : DataState()

    companion object {
        const val STATE_DEFAULT = 0
        const val STATE_EMPTY = 1
        const val STATE_OFFSET_LIMIT = 2
    }

}