package com.anton.forpinch.games.paging.items

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view)