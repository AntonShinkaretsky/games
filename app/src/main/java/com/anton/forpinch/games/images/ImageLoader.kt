package com.anton.forpinch.games.images

import android.widget.ImageView
import com.anton.forpinch.games.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadToList(hash: String) {
    val requestOptions = RequestOptions().error(R.drawable.image_placeholder)
    val url = createUrl(ImageSize.COVER_BIG.size, hash)
    load(url, requestOptions)
}

fun ImageView.loadHeaderImage(hash: String) {
    val requestOptions = RequestOptions().error(R.drawable.image_placeholder)
    val url = createUrl(ImageSize.HD.size, hash)
    load(url, requestOptions)
}

private fun createUrl(size: String, hash: String): String {
    return "https://images.igdb.com/igdb/image/upload/t_$size/$hash.jpg"
}

private fun ImageView.load(url: String, options: RequestOptions) {
    Glide
            .with(context)
            .load(url)
            .apply(options)
            .into(this)
}
