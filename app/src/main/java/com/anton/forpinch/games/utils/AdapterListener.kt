package com.anton.forpinch.games.utils

interface AdapterListener {
    fun listen(click: AdapterClick?)
}
