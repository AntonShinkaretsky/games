package com.anton.forpinch.games

import android.app.Application
import org.koin.android.ext.android.startKoin

class GamesApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(mainModule))
    }

}