package com.anton.forpinch.games.utils

class ActionWithTag(val tag: String) : AdapterClick {

    companion object {
        // Common actions:
        const val ACTION_REFRESH = "action_refresh"
    }

}