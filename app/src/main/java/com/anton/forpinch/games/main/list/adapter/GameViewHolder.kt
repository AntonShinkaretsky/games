package com.anton.forpinch.games.main.list.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.anton.forpinch.games.R
import com.anton.forpinch.games.extensions.gone
import com.anton.forpinch.games.extensions.show
import com.anton.forpinch.games.images.loadToList
import com.anton.forpinch.games.main.model.Game
import kotlinx.android.synthetic.main.item_game.view.*

class GameViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val context = itemView.context

    fun bind(game: Game) {
        itemView.name_game.text = game.name.orEmpty()

        itemView.description_game.text = game.summary
                ?: context.getString(R.string.placeholder_summary)

        val hash = game.cover?.imageHash
        if (hash != null) {
            itemView.image_game.show()
            itemView.image_game.loadToList(hash)
        } else {
            itemView.image_game.gone()
            itemView.image_game.setImageResource(0)
        }
    }

}