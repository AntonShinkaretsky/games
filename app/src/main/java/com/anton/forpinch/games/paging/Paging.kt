package com.anton.forpinch.games.paging

import androidx.paging.PagedList

object Paging {

    const val ID_LOADING = -2
    const val ID_ERROR = -3
    const val ID_OFFSET_LIMITATION = -4

    const val INITIAL_OFFSET = 0
    const val LOAD_SIZE = 20

    val config: PagedList.Config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(LOAD_SIZE)
            .setPageSize(LOAD_SIZE)
            .setPrefetchDistance(10)
            .build()
}