package com.anton.forpinch.games.navigation

import android.os.Bundle
import androidx.navigation.NavOptions

sealed class NavigatorAction {

    data class Forward(
            val frame: Int,
            val action: Int,
            val bundle: Bundle? = null,
            val navOptions: NavOptions? = null
    ) : NavigatorAction()

    data class Back(
            val frame: Int
    ) : NavigatorAction()

    data class BackTo(
            val frame: Int,
            val destination: Int,
            val inclusive: Boolean = false // Also deletes specified fragment from stack
    ) : NavigatorAction()

}