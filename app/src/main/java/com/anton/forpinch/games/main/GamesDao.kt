package com.anton.forpinch.games.main

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*
import com.anton.forpinch.games.main.model.Game

@Dao
interface GamesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<Game>)

    @Query("SELECT * FROM game ORDER BY indexInResponse ASC")
    fun getAllSourceFactory(): DataSource.Factory<Int, Game>

    @Query("SELECT * FROM game WHERE id = :gameId")
    fun get(gameId: Int): LiveData<Game>

    @Query("DELETE FROM game")
    fun deleteAll()

    @Query("SELECT MAX(indexInResponse) + 1 FROM game")
    fun getNextIndexInGames(): Int

}