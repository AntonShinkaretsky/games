package com.anton.forpinch.games.paging.items

import com.anton.forpinch.games.main.list.adapter.GamesItem
import com.anton.forpinch.games.paging.Paging.ID_ERROR

class ItemError : GamesItem {

    override val id: Int
        get() = ID_ERROR

    override fun equals(other: Any?): Boolean {
        return other is ItemError
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}