package com.anton.forpinch.games

import androidx.room.Room
import com.anton.forpinch.games.main.GamesService
import com.anton.forpinch.games.main.details.GameDetailsViewModel
import com.anton.forpinch.games.main.details.repo.GameDetailsRepositoryImp
import com.anton.forpinch.games.main.list.GamesListViewModel
import com.anton.forpinch.games.main.list.repo.GamesListRepositoryImp
import com.anton.forpinch.games.main.navigation.GamesNavigatorImp
import com.anton.forpinch.games.navigation.AppNavigator
import com.anton.forpinch.games.navigation.ViewNavigator
import com.anton.forpinch.games.networking.Retrofit
import com.anton.forpinch.games.room.AppDatabase
import com.anton.forpinch.games.room.AppDatabase.Companion.DATABASE_NAME
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val mainModule = module {

    // Navigation
    single { AppNavigator() }
    factory { (activity: AppActivity) -> ViewNavigator(activity) }

    // Database
    single { Room.databaseBuilder(get(), AppDatabase::class.java, DATABASE_NAME).build() }

    // Service
    single { Retrofit.instance.create(GamesService::class.java) }

    // Could be games module in a bigger app
    viewModel {
        GamesListViewModel(
            GamesNavigatorImp(get()),
            GamesListRepositoryImp(get(), get())
        )
    }
    viewModel {
        GameDetailsViewModel(
            GamesNavigatorImp(get()),
            GameDetailsRepositoryImp(get())
        )
    }

}