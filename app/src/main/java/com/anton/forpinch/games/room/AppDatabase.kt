package com.anton.forpinch.games.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.anton.forpinch.games.main.GamesDao
import com.anton.forpinch.games.main.model.Game

@Database(entities = [Game::class], version = 1)

abstract class AppDatabase : RoomDatabase() {

    abstract fun gamesDao(): GamesDao

    companion object {
        const val DATABASE_NAME = "main-db"
    }

}