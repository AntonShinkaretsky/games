package com.anton.forpinch.games.main.list

import android.os.Bundle
import androidx.lifecycle.ViewModel
import com.anton.forpinch.games.main.list.repo.GamesListRepository
import com.anton.forpinch.games.main.navigation.GamesNavigator

class GamesListViewModel(
        private val navigator: GamesNavigator,
        repo: GamesListRepository
) : ViewModel() {

    private val gamesListing = repo.getGames()

    val posts = gamesListing.pagedList
    val networkState = gamesListing.networkState
    val refreshState = gamesListing.refreshState

    fun refresh() {
        gamesListing.refresh.invoke()
    }

    fun retry() {
        gamesListing.retry.invoke()
    }

    fun moveGamesListToDetails(bundle: Bundle) {
        navigator.moveGamesListToDetails(bundle)
    }

}